//
//  UserService.swift
//  github
//
//  Created by Tirta AR on 03/09/22.
//

import Foundation

protocol IUserService {
    func getUsers(query: String, currentPage: Int, perPage: Int) async -> GeneralResponse<RootResponse<[User]>, ErrorResponse>
}

struct UserService: HTTPClient, IUserService {
    func getUsers(query: String, currentPage: Int, perPage: Int) async -> GeneralResponse<RootResponse<[User]>, ErrorResponse> {
        return await sendRequest(endpoint: UserEndpoint.getUsers(query: query, currentPage: currentPage, perPage: perPage))
    }
}
