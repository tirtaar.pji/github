//
//  NotFoundView.swift
//  github
//
//  Created by Tirta AR on 02/09/22.
//

import Foundation
import UIKit

class NotFoundView {
    
    static let notFound = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 42))
    
    class func show(view: UIView){
        if notFound.isDescendant(of: view) {
            return
        }
        notFound.text = "No Data Found"
        notFound.textAlignment = .center
        //notFound.center = CGPoint(x: view.bounds.width/2, y: view.bounds.height/2)
        view.addSubview(notFound)
        notFound.translatesAutoresizingMaskIntoConstraints = false
        notFound.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        notFound.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    class func showWithCustomText(view: UIView, text : String,  widthCustomText: Float = 1){
        if notFound.isDescendant(of: view) {
           return
        }
        notFound.text = text
        notFound.numberOfLines = 5
        notFound.textAlignment = .center
        //notFound.center = CGPoint(x: view.bounds.width/2, y: view.bounds.height/2)
        view.addSubview(notFound)
        notFound.translatesAutoresizingMaskIntoConstraints = false
        notFound.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        notFound.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        notFound.widthAnchor.constraint(equalToConstant: view.frame.width*CGFloat(widthCustomText)).isActive = true
    }
    
    class func remove(){
        notFound.removeFromSuperview()
    }
    
    
    let notFound = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 42))
    
    func show(view: UIView){
        if notFound.isDescendant(of: view) {
            return
        }
        notFound.text = "No Data Found"
        notFound.textAlignment = .center
        //notFound.center = CGPoint(x: view.bounds.width/2, y: view.bounds.height/2)
        view.addSubview(notFound)
        notFound.translatesAutoresizingMaskIntoConstraints = false
        notFound.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        notFound.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    func showWithCustomText(view: UIView, text : String,  widthCustomText: Float = 1){
        if notFound.isDescendant(of: view) {
           return
        }
        notFound.text = text
        notFound.numberOfLines = 5
        notFound.textAlignment = .center
        //notFound.center = CGPoint(x: view.bounds.width/2, y: view.bounds.height/2)
        view.addSubview(notFound)
        notFound.translatesAutoresizingMaskIntoConstraints = false
        notFound.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        notFound.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        notFound.widthAnchor.constraint(equalToConstant: view.frame.width*CGFloat(widthCustomText)).isActive = true
    }
    
    func remove(){
        notFound.removeFromSuperview()
    }
    
}
