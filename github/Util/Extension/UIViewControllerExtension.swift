//
//  UIViewControllerExtension.swift
//  github
//
//  Created by Tirta AR on 02/09/22.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title: String?, message: String, options: [String], completion: @escaping (Int) -> Void) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.view.accessibilityIdentifier = "alert"
            for (index, option) in options.enumerated() {
                alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                    completion(index)
                }))
            }
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
