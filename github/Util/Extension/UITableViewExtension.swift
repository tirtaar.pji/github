//
//  UITableViewExtension.swift
//  github
//
//  Created by Tirta AR on 04/09/22.
//

import Foundation
import UIKit

extension UITableView {
    func scrollToTop() {
        guard self.numberOfSections > 0 else {
            return
        }
        guard self.numberOfRows(inSection: 0) > 0 else {
            return
        }
        self.scrollToRow(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
    }
}
