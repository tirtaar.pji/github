//
//  HTTPClient.swift
//  github
//
//  Created by Tirta AR on 03/09/22.
//

import Foundation

protocol HTTPClient {
    func sendRequest<T: Decodable, E: Decodable>(endpoint: Endpoint) async -> GeneralResponse<T,E>
}

extension HTTPClient {
    func sendRequest<T: Decodable, E: Decodable>(
        endpoint: Endpoint
    ) async -> GeneralResponse<T, E> {
        var urlComponents = URLComponents()
        urlComponents.scheme = endpoint.scheme
        urlComponents.host = endpoint.host
        urlComponents.path = endpoint.path
        urlComponents.queryItems =  endpoint.queryItems
        
        guard let url = urlComponents.url else {
            return .failure(nil, .invalidURL)
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = endpoint.method.rawValue
        request.allHTTPHeaderFields = endpoint.header

        if let body = endpoint.body {
            request.httpBody = try? JSONSerialization.data(withJSONObject: body, options: [])
        }
        print("Request: \(request)")
        
        do {
            let (data, response) = try await URLSession.shared.data(for: request, delegate: nil)
            guard let response = response as? HTTPURLResponse else {
                return .failure(nil, .noResponse)
            }
            print("Status Code: \(response.statusCode)")
            print("Response : \(String(data: data, encoding: .utf8)!)")
            
            let jsonDecoder = JSONDecoder()
            if let responseModel = try? jsonDecoder.decode(T.self, from: data) {
                return .success(responseModel)
            }else if let responseModel = try? jsonDecoder.decode(E.self, from: data){
                return .failure(responseModel, nil)
            }else {
                return .failure(nil, .decode)
            }
        } catch {
            return .failure(nil, .unknown)
        }
    }
}
