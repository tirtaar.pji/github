//
//  UserEndpoint.swift
//  github
//
//  Created by Tirta AR on 03/09/22.
//

import Foundation

enum UserEndpoint {
    case getUsers(query: String, currentPage: Int, perPage: Int)
}

extension UserEndpoint: Endpoint {
    
    var path: String {
        switch self {
        case .getUsers:
            return "/search/users"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .getUsers:
            return .get
        }
    }

    var header: [String: String]? {
        return nil
    }
    
    var body: [String: String]? {
        return nil
    }
    
    var queryItems: [URLQueryItem]? {
        switch self {
        case .getUsers(let query, let currentPage, let perPage):
            return [
                URLQueryItem(name: "q", value: query),
                URLQueryItem(name: "per_page", value: String(perPage)),
                URLQueryItem(name: "page", value: String(currentPage))
            ]
        }
    }
    
}
