//
//  RootResponse.swift
//  github
//
//  Created by Tirta AR on 01/09/22.
//

import Foundation


struct RootResponse<T: Decodable>: Decodable {
    var totalCount: Int?
    var incompleteResults: Bool = false
    //var items: [T] = []
    var items: T
    
    enum CodingKeys: String, CodingKey {
       case totalCount = "total_count", incompleteResults = "incomplete_results", items
    }
}
