//
//  RequestModel.swift
//  github
//
//  Created by Tirta AR on 01/09/22.
//

import Foundation

struct RequestModel  {
    var scheme: URLScheme
    var host: URLHost
    var path: URLPath
    var method: HTTPMethod
    var querryItems: [URLQueryItem]?
    var httpBody: [String:Any]? = nil
    var params: [String:Any]? = nil
    var httpHeaderField: [String : String]? = nil
}

enum URLScheme: String {
    case https = "https"
    case http = "http"
}

enum URLHost: String {
    case debug = "api.github.com"
}

enum URLPath : String   {
    case searchUsers = "/search/users"
}

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}
