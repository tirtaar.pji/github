//
//  Contracts.swift
//  github
//
//  Created by Tirta AR on 01/09/22.
//

import Foundation
import UIKit

protocol IPagination {
    func isNeedPaging() -> Bool
    var currentPage: Int { get set }
    var currentPageValue: Int { get set }
    var perPage: Int { get set }
    var isLastPage: Bool { get set }
    var isPaging: Bool { get set }
}

protocol IUserInteractor {
    var service: IUserService? { get set }
    var presenter: IUserInteractorOut? { get set }
    func getUsers(isPagination: Bool, query: String, currentPage: Int, perPage: Int) async
}

protocol IUserInteractorOut {
    func fetchSuccess(users: [User])
    func fetchPagingSuccess(users: [User])
    func fetchError(message: String?)
    func fetchPagingError(message: String?)
}

protocol IUserPresenter: IPagination {
    var interactor: IUserInteractor? { get set }
    var view: IView? { get set }
    var router: IUserRouter? { get set }
    
    var users: [User] { get set }
    func getUser(index: Int) -> User
    func numberOfRows() -> Int
    func fetchUsers(query: String?)
    func fetchUsersPaging(query: String?)
}

protocol IUserRouter {
    static func setupModule() -> UINavigationController
}

protocol IView {
    func showLoading()
    func hideLoading()
    func reload()
    func showAlert(title: String?, msg: String, opts: [String])
    func scrollToTop()
}
