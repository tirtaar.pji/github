//
//  ErrorResponse.swift
//  github
//
//  Created by Tirta AR on 02/09/22.
//

import Foundation

struct ErrorResponse: Decodable {
    var message: String?
    var documentationUrl: String?
    
    enum CodingKeys: String, CodingKey {
       case message, documentationUrl = "documentation_url"
    }
}

