//
//  GeneralResponse.swift
//  github
//
//  Created by Tirta AR on 02/09/22.
//

import Foundation

enum GeneralResponse<T,E> {
    case success(T?)
    case failure(E?, RequestError?)
}
