//
//  UserResponse.swift
//  github
//
//  Created by Tirta AR on 01/09/22.
//

import Foundation


struct User: Decodable {
    
    var id: Int?
    var username: String?
    var avatarUrl: String?
    
    enum CodingKeys: String, CodingKey {
       case id = "id", username = "login", avatarUrl = "avatar_url"
    }
    
}
