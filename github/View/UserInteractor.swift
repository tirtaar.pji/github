//
//  UserInteractor.swift
//  github
//
//  Created by Tirta AR on 03/09/22.
//

import Foundation

class UserInteractor: IUserInteractor {
    var service: IUserService?
    var presenter: IUserInteractorOut?
    
    func getUsers(isPagination: Bool, query: String, currentPage: Int, perPage: Int) async {
        guard let result = await service?.getUsers(query: query, currentPage: currentPage, perPage: perPage) else {
            return
        }
        switch result {
        case .success(let result):
            if isPagination {
                presenter?.fetchPagingSuccess(users: result?.items ?? [])
            }else {
                presenter?.fetchSuccess(users: result?.items ?? [])
            }
        case .failure(let error, let requestError):
            presenter?.fetchError(message: error?.message ?? requestError?.customMessage)
        }
    }
    
}
