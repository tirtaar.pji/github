//
//  Router.swift
//  github
//
//  Created by Tirta AR on 01/09/22.
//

import Foundation
import UIKit

class UserRouter: IUserRouter {
    
    static func setupModule() -> UINavigationController {
        let viewController = UserVC()
        let navigationController = UINavigationController(rootViewController: viewController)
        let router = UserRouter()
        let userService = UserService()
        let presenter: IUserPresenter & IUserInteractorOut = UserPresenter()
        
        viewController.presenter = presenter
        viewController.presenter?.router = router
        viewController.presenter?.view = viewController
        
        let userInteractor = UserInteractor()
        userInteractor.service = userService
        userInteractor.presenter = presenter
        
        viewController.presenter?.interactor = userInteractor
        
        return navigationController
    }
    
    
}
