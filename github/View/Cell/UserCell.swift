//
//  UserCell.swift
//  github
//
//  Created by Tirta AR on 01/09/22.
//

import Foundation
import UIKit

class UserCell: UITableViewCell {

    let imgAvatar = UIImageView()
    let lbUsername = UILabel()

    public static let identifier = "userCell"

    required init?(coder: NSCoder) {fatalError("init(coder:) has not been implemented")}

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        awakeFromNib()
    }
    
    override func awakeFromNib() {
        lbUsername.font = UIFont.boldSystemFont(ofSize: 18)
        contentView.addSubview(imgAvatar)
        contentView.addSubview(lbUsername)
        
        imgAvatar.translatesAutoresizingMaskIntoConstraints = false
        imgAvatar.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.8) .isActive = true
        imgAvatar.widthAnchor.constraint(equalTo: imgAvatar.heightAnchor, multiplier: 1).isActive = true
        imgAvatar.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        imgAvatar.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        
        lbUsername.translatesAutoresizingMaskIntoConstraints = false
        lbUsername.leadingAnchor.constraint(equalTo: imgAvatar.trailingAnchor, constant: 20).isActive = true
        lbUsername.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        lbUsername.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 20).isActive = true
        
    }
}
