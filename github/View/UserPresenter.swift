//
//  Presenter.swift
//  github
//
//  Created by Tirta AR on 01/09/22.
//

import Foundation

class UserPresenter: IUserPresenter {
    
    var view: IView?
    var router: IUserRouter?
    var interactor: IUserInteractor?
    
    var users: [User] = []
    var isLastPage: Bool = false
    var currentPage: Int {
        get {
            return currentPageValue
        }
        set(newValue) {
            if newValue == 0 {
                currentPageValue = 1
            }else {
                currentPageValue = newValue
            }
        }
    }
    var currentPageValue: Int = 1
    var perPage: Int = 10
    var isPaging: Bool = false
    
    func isNeedPaging() -> Bool {
        return isLastPage || numberOfRows() == 0
    }
    
    func getUser(index: Int) -> User {
        return users[index]
    }
    
    func numberOfRows() -> Int {
        return users.count
    }
    
    func fetchUsers(query: String?) {
        guard let query = query, !query.isEmpty else {
            self.users.removeAll()
            view?.reload()
            view?.hideLoading()
            return
        }
        isPaging = false
        currentPage = 1
        isLastPage = false
        Task {
            await interactor?.getUsers(isPagination: false, query: query, currentPage: currentPage, perPage: perPage)
        }
    }
    
    func fetchUsersPaging(query: String?) {
        guard let query = query, !query.isEmpty else {
            return
        }
        guard !isPaging && !isLastPage else {
            return
        }
        isPaging = true
        currentPage += 1
        Task {
            await interactor?.getUsers(isPagination: true, query: query, currentPage: currentPage, perPage: perPage)
        }
    }
    
}

extension UserPresenter: IUserInteractorOut {
    func fetchSuccess(users: [User]) {
        self.users = users
        view?.reload()
        view?.scrollToTop()
        view?.hideLoading()
    }
    
    func fetchPagingSuccess(users: [User]) {
        guard isPaging else {
            return
        }
        isPaging = false
        guard !users.isEmpty else {
            isLastPage = true
            currentPage -= 1
            view?.reload()
            return
        }
        self.users.append(contentsOf: users)
        view?.reload()
    }
    
    func fetchError(message: String?) {
        view?.showAlert(title: nil, msg: message ?? "", opts: ["OK"])
        view?.hideLoading()
    }
    
    func fetchPagingError(message: String?) {
        isPaging = false
        currentPage -= 1
        view?.showAlert(title: nil, msg: message ?? "", opts: ["OK"])
    }
    
}
