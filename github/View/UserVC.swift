//
//  ViewController.swift
//  github
//
//  Created by Tirta AR on 01/09/22.
//

import UIKit

class UserVC: UIViewController {
    
    var searchBar: UISearchBar?
    var tableView: UITableView?
    var refreshControl: UIRefreshControl!
    private var spinner = UIActivityIndicatorView(style: .medium)
    
    var presenter: IUserPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchBar()
        setupTableView()
    }
    
    func setupSearchBar() {
        searchBar = UISearchBar()
        searchBar?.sizeToFit()
        searchBar?.accessibilityIdentifier = "searchBar"
        navigationItem.titleView = searchBar
        searchBar?.delegate = self
    }
    
    func setupTableView() {
        tableView = UITableView()
        refreshControl = UIRefreshControl()
        refreshControl.accessibilityIdentifier = "refreshControl"
        refreshControl.addTarget(self, action: #selector(doRefresh), for: .valueChanged)
        tableView?.addSubview(refreshControl)
        view?.addSubview(tableView ?? UITableView())
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.translatesAutoresizingMaskIntoConstraints = false
        tableView?.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        tableView?.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        tableView?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        tableView?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        tableView?.register(UserCell.self, forCellReuseIdentifier: UserCell.identifier)
    }
    
    @objc func doRefresh() {
        if refreshControl.isRefreshing {
            presenter?.fetchUsers(query: searchBar?.text)
        }
    }
    
    func fetchPaging() {
        presenter?.fetchUsersPaging(query: searchBar?.text)
    }
    
}

extension UserVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserCell.identifier, for: indexPath) as? UserCell else {
            fatalError("Unable to dequeue libraryTableCell")
        }
        let user = presenter?.getUser(index: indexPath.row)
        cell.lbUsername.text = user?.username
        UIImage.loadFrom(url: URL(string: user?.avatarUrl ?? "")!) { image in
            DispatchQueue.main.async {
                cell.imgAvatar.image = image
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if presenter?.isNeedPaging() ?? false {
            spinner.stopAnimating();
            return
        }
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            spinner.startAnimating()
            spinner.accessibilityIdentifier = "spinner"
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            spinner.hidesWhenStopped = true
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
            if !(presenter?.isPaging ?? false) {
                fetchPaging()
            }
            
        }
    }
    
}

extension UserVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.searchBarDidChange),
            object: searchBar)
        self.perform(
            #selector(self.searchBarDidChange),
            with: searchBar,
            afterDelay: 1.5)
    }
    
    @objc func searchBarDidChange() {
        showLoading()
    }
    
}

extension UserVC: IView {
    func showAlert(title: String?, msg: String, opts: [String]) {
        DispatchQueue.main.async {
            self.showAlert(title: title, message: msg, options: opts) { _ in }
        }
    }
    
    func showLoading() {
        DispatchQueue.main.async {
            self.refreshControl.beginRefreshingManually()
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
            self.spinner.stopAnimating()
        }
    }
    
    func reload() {
        DispatchQueue.main.async {
            self.tableView?.reloadData()
        }
    }
    
    func scrollToTop() {
        DispatchQueue.main.async {
            self.tableView?.scrollToTop()
        }
    }
    
}

