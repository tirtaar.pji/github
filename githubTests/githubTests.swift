//
//  githubTests.swift
//  githubTests
//
//  Created by Tirta AR on 01/09/22.
//

import XCTest
@testable import github

class MockUserService: Mockable, IUserService {
    
    var isResponseSuccess: Bool = false
    
    func getUsers(query: String, currentPage: Int, perPage: Int) async -> GeneralResponse<RootResponse<[User]>, ErrorResponse> {
        if isResponseSuccess {
            let response = loadJSON(filename: "user_response", type: RootResponse<[User]>.self)
            var items: [User] = []
            let offset = ((currentPage-1)*perPage)
            let limit = currentPage*perPage
            for (index, _item) in response.items.enumerated() {
                if (index >= offset) && (index < limit) {
                    items.append(_item)
                }
            }
            let result = RootResponse(items: items)
            return .success(result)
        }else {
            return .failure(ErrorResponse(message: "API Exceed Limit", documentationUrl: nil), nil)
        }
    }
}

class MockUserInteractor: IUserInteractor {
    var service: IUserService?
    var presenter: IUserInteractorOut?
    
    func getUsers(isPagination: Bool, query: String, currentPage: Int, perPage: Int) async {
        guard let result = await service?.getUsers(query: query, currentPage: currentPage, perPage: perPage) else {
            return
        }
        switch result {
        case .success(let result):
            XCTAssertNotNil(result?.items)
            if isPagination {
                presenter?.fetchPagingSuccess(users: result?.items ?? [])
            }else {
                presenter?.fetchSuccess(users: result?.items ?? [])
            }
        case .failure(let error, let requestError):
            XCTAssertNotNil(error ?? requestError)
            presenter?.fetchError(message: error?.message ?? requestError?.customMessage)
        }
    }
}

class MockUserPresenter: IUserPresenter {
    var interactor: IUserInteractor?
    var view: IView?
    var router: IUserRouter?
    
    var users: [User] = []
    var currentPage: Int {
        get {
            return currentPageValue
        }
        set(newValue) {
            if newValue == 0 {
                currentPageValue = 1
            }else {
                currentPageValue = newValue
            }
        }
    }
    var currentPageValue: Int = 1
    var perPage: Int = 10
    var isLastPage: Bool = false
    var isPaging: Bool = false
    
    func getUser(index: Int) -> User {
        return users[index]
    }
    
    func numberOfRows() -> Int {
        return users.count
    }
    
    func fetchUsers(query: String?) {
        Task {
           await interactor?.getUsers(isPagination: false, query: query ?? "", currentPage: currentPage, perPage: perPage)
        }
        
    }
    
    func fetchUsersPaging(query: String?) {
        Task {
            await interactor?.getUsers(isPagination: true, query: query ?? "", currentPage: currentPage, perPage: perPage)
        }
        
    }
    
    func isNeedPaging() -> Bool {
        return isLastPage || numberOfRows() == 0
    }
    
    var msgError: String?
    var isPagingCalled: Bool = false
    var isRefreshCalled: Bool = false
    
}

extension MockUserPresenter: IUserInteractorOut {
    
    func fetchSuccess(users: [User]) {
        isRefreshCalled = true
        self.users = users
        view?.reload()
        view?.hideLoading()
    }
    
    func fetchPagingSuccess(users: [User]) {
        isPagingCalled = true
        self.users.append(contentsOf: users)
        view?.reload()
    }
    
    func fetchError(message: String?) {
        msgError = message
        view?.showAlert(title: nil, msg: message ?? "", opts: ["OK"])
    }
    
    func fetchPagingError(message: String?) {
        msgError = message
        view?.showAlert(title: nil, msg: message ?? "", opts: ["OK"])
    }
    
}

class MockUserVC: IView {
    var loadingShowed: Bool = false
    var tableReloaded: Bool = false
    var messageError: String?
    var tableScrolledToTop: Bool = false
    
    func showLoading() {
        loadingShowed = true
    }
    
    func hideLoading() {
        loadingShowed = false
    }
    
    func reload() {
        tableReloaded = true
    }
    
    func showAlert(title: String?, msg: String, opts: [String]) {
        messageError = msg
    }
    
    func scrollToTop() {
        tableScrolledToTop = true
    }
    
    
}

class UserInteractorTests: XCTestCase {
    
    var sut: UserInteractor!
    var mockService: MockUserService!
    var mockPresenter: MockUserPresenter!
    
    override func setUpWithError() throws {
        mockService = MockUserService()
        mockPresenter = MockUserPresenter()
        sut = UserInteractor()
        sut.service = mockService
        sut.presenter = mockPresenter
    }

    override func tearDownWithError() throws {
        mockService = nil
        mockPresenter = nil
        sut = nil
    }

    func testSuccessFetchResponse() async throws {
        mockService.isResponseSuccess = true
        await sut.getUsers(isPagination: false, query: "Pikachu", currentPage: 1, perPage: 10)
        XCTAssertTrue(mockPresenter.isRefreshCalled)
        XCTAssertEqual(mockPresenter.users.count, 10)
    }
    
    func testSuccessPagingFetchResponse() async throws {
        mockService.isResponseSuccess = true
        await sut.getUsers(isPagination: true, query: "Pikachu", currentPage: 1, perPage: 10)
        await sut.getUsers(isPagination: true, query: "Pikachu", currentPage: 2, perPage: 10)
        XCTAssertTrue(mockPresenter.isPagingCalled)
        XCTAssertEqual(mockPresenter.users.count, 17)
    }
    
    func testFailureFetchResponse() async throws {
        mockService.isResponseSuccess = false
        await sut.getUsers(isPagination: false, query: "Pikachu", currentPage: 1, perPage: 10)
        XCTAssertNotNil(mockPresenter.msgError)
    }
    
    func testFailureFetchPagingResponse() async throws {
        mockService.isResponseSuccess = false
        await sut.getUsers(isPagination: true, query: "Pikachu", currentPage: 1, perPage: 10)
        await sut.getUsers(isPagination: true, query: "Pikachu", currentPage: 2, perPage: 10)
        XCTAssertNotNil(mockPresenter.msgError)
    }

}

class UserPresenterTests: XCTestCase {
    
    var sut: UserPresenter!
    var mockInteractor: MockUserInteractor!
    var mockInteractorOut: UserPresenter!
    var mockView: MockUserVC!
    var mockService: MockUserService!
    
    override func setUpWithError() throws {
        sut = UserPresenter()
        mockService = MockUserService()
        mockInteractor = MockUserInteractor()
        mockView = MockUserVC()
        mockInteractor.presenter = sut
        mockInteractor.service = mockService
        sut.interactor = mockInteractor
        sut.view = mockView
    }

    override func tearDownWithError() throws {
        mockService = nil
        mockInteractor = nil
        mockView = nil
        sut = nil
    }
    
    func testCurrentPageWhenRefresh() throws {
        mockService.isResponseSuccess = true
        sut.fetchUsers(query: "Pikachu")
        waitExpectationSync()
        XCTAssertEqual(self.sut.currentPage, 1)
    }

    func testCurrentPageWhenPaging() throws {
        mockService.isResponseSuccess = true
        sut.fetchUsersPaging(query: "Pikachu")
        waitExpectationSync()
        XCTAssertEqual(self.sut.currentPage, 2)
    }
    
    func testUsersCountWhenRefresh() throws {
        mockService.isResponseSuccess = true
        sut.fetchUsers(query: "Pikachu")
        waitExpectationSync()
        XCTAssertEqual(self.sut.users.count, 10)
    }
    
    func testUsersCountWhenPaging() throws {
        mockService.isResponseSuccess = true
        sut.fetchUsers(query: "Pikachu")
        waitExpectationSync()
        sut.fetchUsersPaging(query: "Pikachu")
        waitExpectationSync()
        XCTAssertEqual(sut.users.count, 17)
    }
    
    func testShowLoadingHideAfterFetchUsers() {
        mockService.isResponseSuccess = true
        sut.fetchUsers(query: "Pikachu")
        waitExpectationSync()
        XCTAssertFalse(mockView.loadingShowed)
    }
    
    func testTableScrollToTopAfterFetchUsers() {
        mockService.isResponseSuccess = true
        sut.fetchUsers(query: "Pikachu")
        waitExpectationSync()
        XCTAssertTrue(mockView.tableScrolledToTop)
    }
    
}


class UserVCTests: XCTestCase {
    
    var sut: UserVC!
    var mockPresenter: MockUserPresenter!
    var mockInteractor: MockUserInteractor!
    var mockService: MockUserService!
    
    override func setUpWithError() throws {
        sut = UserVC()
        mockService = MockUserService()
        mockPresenter = MockUserPresenter()
        mockInteractor = MockUserInteractor()
        mockInteractor.service = mockService
        mockInteractor.presenter = mockPresenter
        mockPresenter.interactor = mockInteractor
        mockPresenter.view = sut
        sut.presenter = mockPresenter
        sut.setupSearchBar()
        sut.setupTableView()
        
    }

    override func tearDownWithError() throws {
        mockService = nil
        mockInteractor = nil
        mockPresenter = nil
        sut = nil
    }
    
    func testLoadingShowedWhenGetUsers() throws {
        sut.showLoading()
        waitExpectationSync()
        XCTAssertTrue(self.sut.refreshControl.isRefreshing)
    }
    
    func testCountTableNumberOfRowsAfterFetchUsers() throws {
        mockService.isResponseSuccess = true
        sut.showLoading()
        waitExpectationSync()
        XCTAssertEqual(self.sut.tableView!.numberOfRows(inSection: 0), 10)
    }
    
}
