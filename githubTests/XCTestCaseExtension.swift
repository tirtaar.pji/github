//
//  XCTestCaseExtension.swift
//  githubTests
//
//  Created by Tirta AR on 06/09/22.
//

import XCTest

extension XCTestCase {
    func waitExpectationSync(timeInterval: TimeInterval = 5, timeOut: TimeInterval = 10, expectationDesc: String = "") {
        let expectation = XCTestExpectation(description: expectationDesc)
        DispatchQueue.global().async {
            Thread.sleep(forTimeInterval: timeInterval)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: timeOut)
    }
}
