//
//  githubUITests.swift
//  githubUITests
//
//  Created by Tirta AR on 01/09/22.
//

import XCTest

extension XCUIElement {
    func scrollToElement(element: XCUIElement) {
        while !element.display() {
            swipeUp()
        }
    }
    
//    func scrollToElement(element: XCUIElement)
//    {
//        while element.visible() == false
//        {
//            let app = XCUIApplication()
//            let startCoord = app.collectionViews.element.coordinate(withNormalizedOffset: CGVector(dx: 0.5, dy: 0.5))
//            let endCoord = startCoord.withOffset(CGVector(dx: 0.0, dy: -262));
//            startCoord.press(forDuration: 0.01, thenDragTo: endCoord)
//        }
//    }

    func visible() -> Bool {
        guard self.exists && !self.frame.isEmpty else { return false }
        return XCUIApplication().windows.element(boundBy: 0).frame.contains(self.frame)
    }
    
    func display() -> Bool {
        return self.exists
    }
}


class githubUITests: XCTestCase {

    func testShowLoading() {
        let app = XCUIApplication()
        app.launch()
        let searchField = app.descendants(matching: .any)["searchBar"]
        XCTAssertTrue(searchField.exists)
        searchField.tap()
        searchField.typeText("Manila")
        
        XCTAssertTrue(app.descendants(matching: .any)["refreshControl"].waitForExistence(timeout: 2))
                             
    }
    
    func testSucessSearchQuery() {
        let app = XCUIApplication()
        app.launch()
        let searchField = app.descendants(matching: .any)["searchBar"]
        XCTAssertTrue(searchField.exists)
        searchField.tap()
        searchField.typeText("Pikachu")
        
        let tablesQuery = app.tables
        XCTAssertTrue(tablesQuery.cells.element.waitForExistence(timeout: 4))
    }
    
    func testRemoveSearchQuery() {
        let app = XCUIApplication()
        app.launch()
        let searchField = app.descendants(matching: .any)["searchBar"]
        XCTAssertTrue(searchField.exists)
        searchField.tap()
        searchField.typeText("Tokyo")
        
        let tablesQuery = app.tables
        XCTAssertTrue(tablesQuery.cells.element.waitForExistence(timeout: 4))
        app.navigationBars["github.UserVC"].searchFields.buttons["Clear text"].tap()
        
        expectation(for: NSPredicate(format: "exists == 0"), evaluatedWith: tablesQuery.cells.element)
        waitForExpectations(timeout: 4)
        
        XCTAssertFalse(tablesQuery.cells.element.exists)
    }

}
